using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineController : MonoBehaviour
{
    public Renderer rend; // Referencia al Renderer del objeto
    public Material outlineMaterial; // Material con el shader que tiene el contorno
    public Color selectedOutlineColor = Color.black; // Color del contorno cuando est� seleccionado
    public Color unselectedOutlineColor; // Color del cubo para cuando no est� seleccionado

    void Start()
    {
        if (rend == null)
        {
            rend = GetComponent<Renderer>();
        }

        // Asegurarse de que el material de contorno est� asignado desde el inspector
        if (rend != null && outlineMaterial != null)
        {
            // Asignar el material de contorno al Renderer
            Material[] materials = rend.materials;
            materials[1] = outlineMaterial; // Se asume que el contorno es el segundo material
            rend.materials = materials;

            // Obtener el color del cubo como el color del contorno no seleccionado
            unselectedOutlineColor = rend.materials[0].color;

            // Inicializar el color del contorno
            SetOutlineColor(false);
        }
    }

    public void SetOutlineColor(bool isSelected)
    {
        if (rend != null && outlineMaterial != null)
        {
            if (isSelected)
            {
                rend.materials[1].SetColor("_OutlineColor", selectedOutlineColor);
            }
            else
            {
                rend.materials[1].SetColor("_OutlineColor", unselectedOutlineColor);
            }
        }
    }
}
