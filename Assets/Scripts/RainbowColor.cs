using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RainbowColor : MonoBehaviour
{
    public float speed = 1.0f; // Velocidad de cambio de color
    private Renderer rend; // Referencia al Renderer del objeto

    void Start()
    {
        rend = GetComponent<Renderer>();
    }

    void Update()
    {
        // Calcular el color del arco�ris usando el tiempo
        Color rainbowColor = Color.HSVToRGB(Mathf.Repeat(Time.time * speed, 1f), 1f, 1f);

        // Aplicar el color al material del objeto
        rend.material.color = rainbowColor;
    }
}