using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class WinCondition : MonoBehaviour
{
    public GameObject win;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            win.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
