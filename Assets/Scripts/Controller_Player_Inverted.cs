﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Inverted : Controller_Player
{
    private bool isOnFloor;

    public override void FixedUpdate()
    {
        base.rb.AddForce(new Vector3(0, 30f, 0)); // Aplicar fuerza hacia arriba constantemente
        base.FixedUpdate();
    }

    public override void Jump()
    {
        if (isOnFloor && Input.GetKeyDown(KeyCode.W))
        {
            rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse); // Aplicar fuerza hacia abajo al saltar
        }
    }

    public override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            isOnFloor = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            isOnFloor = false;
        }
    }
}
