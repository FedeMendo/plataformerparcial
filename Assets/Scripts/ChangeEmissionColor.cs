using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeEmissionColor : MonoBehaviour
{
    public Renderer rend; // Referencia al Renderer del objeto
    public Color newEmissionColor = Color.red; // Color de emisi�n deseado

    void Start()
    {
        if (rend == null)
        {
            rend = GetComponent<Renderer>();
        }

        // Asegurarse de que el material tiene emisi�n activada y es editable
        rend.material.EnableKeyword("_EMISSION");

        // Cambiar el color de emisi�n del material
        SetEmissionColor(newEmissionColor);
    }

    public void SetEmissionColor(Color color)
    {
        if (rend != null && rend.material != null)
        {
            // Establecer el color de emisi�n del material
            rend.material.SetColor("_EmissionColor", color);

            // Refrescar los cambios en el material para que se apliquen
            rend.material.globalIlluminationFlags = MaterialGlobalIlluminationFlags.RealtimeEmissive;
        }
    }
}