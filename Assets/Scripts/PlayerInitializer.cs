using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInitializer : MonoBehaviour
{
    public GameObject[] playerPrefabs;

    private void Start()
    {
        if (GameManager.Instance != null)
        {
            GameManager.Instance.players.Clear();

            foreach (GameObject playerPrefab in playerPrefabs)
            {
                GameObject playerInstance = Instantiate(playerPrefab);
                Controller_Player controllerPlayer = playerInstance.GetComponent<Controller_Player>();
                if (controllerPlayer != null)
                {
                    GameManager.Instance.players.Add(controllerPlayer);
                }
                else
                {
                    Debug.LogError("Player prefab is missing Controller_Player component.");
                }
            }
        }
    }
}