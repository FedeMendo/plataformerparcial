﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Floating : Controller_Player
{
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Verificar que el otro jugador esté encima del jugador flotante
            if (other.transform.position.y > transform.position.y + GetComponent<Collider>().bounds.size.y / 2)
            {
                other.transform.SetParent(transform);
            }
            else
            {
                other.transform.SetParent(null); // Liberar si no está encima
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.SetParent(null);
        }
    }

    public override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            onFloor = true;
        }

        // Este jugador es invulnerable al agua, por lo que no se destruye en el agua.
    }
}