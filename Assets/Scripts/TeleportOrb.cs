using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportOrb : MonoBehaviour
{
    public Transform[] respawnPoints; // Array de puntos de respawn

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            TeleportAllPlayers();
        }
    }

    private void TeleportAllPlayers()
    {
        if (GameManager.Instance != null && respawnPoints.Length >= GameManager.Instance.players.Count)
        {
            for (int i = 0; i < GameManager.Instance.players.Count; i++)
            {
                GameManager.Instance.players[i].transform.position = respawnPoints[i].position;
            }
            Debug.Log("All players teleported to new positions.");
        }
        else
        {
            Debug.LogError("Not enough respawn points assigned or GameManager instance is missing.");
        }
    }
}