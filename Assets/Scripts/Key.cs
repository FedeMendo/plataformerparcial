using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public GameObject wallToDeactivate; // Referencia a la pared que se va a desactivar

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Desactivar la pared si est� configurada
            if (wallToDeactivate != null)
            {
                wallToDeactivate.SetActive(false);
                Debug.Log("Pared desactivada por la llave.");
            }

            // Destruir el cubo (opcional, si la llave debe ser usada una sola vez)
            Destroy(gameObject);
        }
    }
}