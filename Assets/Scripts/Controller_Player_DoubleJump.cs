﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_DoubleJump : Controller_Player
{
    private int jumpCounter = 2; // Contador máximo de saltos

    public override void Jump()
    {
        if (Input.GetKeyDown(KeyCode.W) && jumpCounter > 0)
        {
            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            jumpCounter--; // Reducir el contador después de cada salto
        }
    }

    public override void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);

        // Reiniciar el contador de saltos cuando el jugador toca el suelo (Floor)
        if (collision.gameObject.CompareTag("Floor"))
        {
            jumpCounter = 2; // Reiniciar el contador a 2 (doble salto disponible)
        }
    }
}