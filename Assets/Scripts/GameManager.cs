﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public static bool gameOver = false;
    public static bool winCondition = false;
    public static int actualPlayer = 0;
    public List<Controller_Player> players;
    public GameObject gameOverText; // Referencia al texto de "Game Over"

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject); // Mantener este objeto vivo al cambiar de escena
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        Physics.gravity = new Vector3(0, -30, 0);
        gameOver = false;
        winCondition = false;
        SetConstraints();
        gameOverText.SetActive(false); // Asegurarse de que el texto esté desactivado al inicio
        UpdateOutlineColors(); // Actualizar colores del contorno al inicio
        actualPlayer = 0;
    }

    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            actualPlayer = 0;
            SetConstraints();
            UpdateOutlineColors();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            actualPlayer = 1;
            SetConstraints();
            UpdateOutlineColors();
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            actualPlayer = 2;
            SetConstraints();
            UpdateOutlineColors();
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            actualPlayer = 3;
            SetConstraints();
            UpdateOutlineColors();
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            actualPlayer = 4;
            SetConstraints();
            UpdateOutlineColors();
        }
        if (Input.GetKeyDown(KeyCode.Alpha6)) // For the new Wall Climber character
        {
            actualPlayer = 5;
            SetConstraints();
            UpdateOutlineColors();
        }
    }

    private void SetConstraints()
    {
        foreach (Controller_Player p in players)
        {
            if (p == players[actualPlayer])
            {
                p.rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            }
            else
            {
                p.rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            }
        }
    }

    private void UpdateOutlineColors()
    {
        for (int i = 0; i < players.Count; i++)
        {
            OutlineController outlineController = players[i].GetComponent<OutlineController>();
            if (outlineController != null)
            {
                outlineController.SetOutlineColor(i == actualPlayer);
            }
        }
    }

    public void TriggerGameOver()
    {
        gameOver = true;
        gameOverText.SetActive(true); // Mostrar el texto de "Game Over"
        Time.timeScale = 0; // Detener el juego
        Debug.Log("Game Over");
    }
}
