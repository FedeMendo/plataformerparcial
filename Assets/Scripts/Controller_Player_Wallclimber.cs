using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Wallclimber : Controller_Player
{
    public float wallClimbSpeed = 10f;
    private bool isWallClimbing = false;

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (GameManager.actualPlayer == playerNumber)
        {
            WallClimb();
        }
    }

    private void WallClimb()
    {
        if (isWallClimbing)
        {
            float climb = Input.GetAxis("Vertical") * wallClimbSpeed;
            rb.velocity = new Vector3(rb.velocity.x, climb, 0);
        }
    }

    public override void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);
        if (collision.gameObject.CompareTag("Wall"))
        {
            isWallClimbing = true;
            rb.useGravity = false;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            isWallClimbing = false;
            rb.useGravity = true;
        }
    }
}