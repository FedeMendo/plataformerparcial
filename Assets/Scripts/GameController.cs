using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            LevelManager.Instance.ReloadCurrentLevel();
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            LevelManager.Instance.LoadNextLevel();
        }
    }
}
