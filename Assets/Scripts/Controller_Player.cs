﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    public float jumpForce = 10;
    public float speed = 5;
    public int playerNumber;
    public Rigidbody rb;
    private BoxCollider col;
    public LayerMask floor;
    internal bool onFloor;
    private int floorContacts = 0; // Contador de contactos con el suelo

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<BoxCollider>();
        rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
        initializePlayer();
    }

    public virtual void FixedUpdate()
    {
        if (GameManager.actualPlayer == playerNumber)
        {
            Movement();
        }
    }

    private void Update()
    {
        if (GameManager.actualPlayer == playerNumber)
        {
            Jump();
        }
    }

    private void initializePlayer()
    {
        onFloor = true;
        floorContacts = 1; // Iniciar con un contacto en el suelo
    }

    private void Movement()
    {
        if (Input.GetKey(KeyCode.A))
        {
            rb.velocity = new Vector3(-speed, rb.velocity.y, 0);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            rb.velocity = new Vector3(speed, rb.velocity.y, 0);
        }
        else
        {
            rb.velocity = new Vector3(0, rb.velocity.y, 0);
        }
    }

    public virtual void Jump()
    {
        if (onFloor && Input.GetKeyDown(KeyCode.W))
        {
            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            onFloor = false; // Marcar que ya no está en el suelo después de saltar
            floorContacts = 0; // Reiniciar el contador de contactos
        }
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Water"))
        {
            GameManager.Instance.TriggerGameOver(); // Llamar a la condición de game over
        }
        if (collision.gameObject.CompareTag("Floor") || collision.gameObject.CompareTag("Player"))
        {
            floorContacts++; // Incrementar el contador de contactos al tocar el suelo o un jugador
            onFloor = true; // Marcar que está en el suelo
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor") || collision.gameObject.CompareTag("Player"))
        {
            floorContacts--; // Decrementar el contador de contactos al dejar de tocar el suelo o un jugador
            if (floorContacts <= 0)
            {
                onFloor = false; // Marcar que ya no está en el suelo si no hay más contactos
                floorContacts = 0; // Asegurarse de que el contador no sea negativo
            }
        }
    }
}